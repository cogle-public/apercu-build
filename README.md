# apercu-build

This is the project to hold the production web front-end for the apercu-service.  It consistes of just 2 files:

```
bundle.js - the javascript code for the project
styles.css - the css for the project
```

The apercu-service will clone the apercu-build project (on container build) to serve up the apercu study.

### Dependencies

The apercu-build is created by the apercu-frontend project.
